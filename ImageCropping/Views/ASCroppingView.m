//
//  ASCroppingView.m
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import "ASCroppingView.h"

@implementation ASCroppingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGFloat width = 200;
    CGFloat height = 200;
    CGFloat x = (self.frame.size.width - width) * 0.5f;
    CGFloat y = (self.frame.size.height - height) * 0.5f;
    CGRect rectangle = CGRectMake(x, y, width, height);
    CGContextAddRect(context, rectangle);
    
    CGContextStrokePath(context);
    
    [[[UIColor blackColor] colorWithAlphaComponent:0.2] setFill];
    
    CGContextSetBlendMode(context, kCGBlendModeClear);
}


@end
