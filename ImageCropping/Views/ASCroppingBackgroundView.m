//
//  ASCroppingView.m
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import "ASCroppingBackgroundView.h"
#import "ASCroppingView.h"
//#import <QuartzCore/QuartzCore.h>

@interface ASCroppingBackgroundView ()
@property (nonatomic, strong) ASCroppingView *croppingView;
@property (nonatomic) CGRect frameRect;
@property (nonatomic) CGPoint pointPressed;
@end

@implementation ASCroppingBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:0.0f
                                               green:0.0f
                                                blue:0.0f
                                               alpha:0.7f];
        CGFloat width = 200;
        CGFloat height = 200;
        CGFloat x = (self.frame.size.width - width) * 0.5f;
        CGFloat y = (self.frame.size.height - height) * 0.5f;
        self.frameRect = CGRectMake(x, y, width, height);
        
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPressGestureRecognizer.minimumPressDuration = 0.0f;
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return self;
}

#pragma Lazy Instantation
- (ASCroppingView *)croppingView
{
    if (!_croppingView)
    {
        _croppingView = [[ASCroppingView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    }
    
    return _croppingView;
}

#pragma mark - View lifecycle & drawing

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
}

- (void)didMoveToSuperview
{
	[super didMoveToSuperview];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(context, 1.0, 0.0, 0.0, 1.0);
    CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0);
    CGContextSetBlendMode(context, kCGBlendModeClear);
    CGContextFillRect(context, self.frameRect);
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress
{
    CGPoint point = [longPress locationInView:self];
    float minX = self.frameRect.origin.x;
    float minY = self.frameRect.origin.y;
    float maxX = self.frame.size.width - self.frameRect.origin.x;
    float maxY = self.frame.size.height - self.frameRect.origin.y;
    
    if (point.x >= minX && point.x <= maxX &&
        point.y >= minY && point.y <= maxY)
    {
        self.frameRect = CGRectMake(point.x - self.frameRect.size.width / 2,
                                    point.y - self.frameRect.size.height / 2,
                                    self.frameRect.size.width,
                                    self.frameRect.size.height);
        [self setNeedsDisplay];

    }
}

@end
