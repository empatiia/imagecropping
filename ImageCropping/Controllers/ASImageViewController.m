//
//  ASImageViewController.m
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import "ASImageViewController.h"
#import "ASCroppingBackgroundView.h"


@interface ASImageViewController ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) ASCroppingBackgroundView *croppingView;
@end

//#define kCroppingViewInitialSize

@implementation ASImageViewController

- (id)initWithImage:(UIImage *)image
{
    self = [super init];
    if (self)
    {
        _image = image;
    }
    
    return self;
}

#pragma mark - View Lifecycle
- (void)loadView
{
    [super loadView];
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-pattern"]];
    self.view = view;
}

- (void)viewDidLoad
{
    self.title = NSLocalizedString(@"CROP", nil);
    UIImage *cropImage = [UIImage imageNamed:@"crop"];
    UIBarButtonItem *cropBarButtonItem = [[UIBarButtonItem alloc] initWithImage:cropImage
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(cropPressed)];
    self.navigationItem.rightBarButtonItem = cropBarButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.imageView removeFromSuperview];
    [self.view addSubview:self.imageView];
}

#pragma mark - Lazy Instantation
- (UIImageView *)imageView
{
    if (!_image)
    {
        return nil;
    }
    
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithImage:self.image];
        _imageView.frame = self.view.frame;
    }
    
    return _imageView;
}

- (ASCroppingBackgroundView *)croppingView
{
    if (!_croppingView)
    {
        _croppingView = [[ASCroppingBackgroundView alloc] initWithFrame:self.view.frame];
    }
    
    return _croppingView;
}

#pragma mark - Private Methods
- (void)cropPressed
{
    [self.view addSubview:self.croppingView];
}

@end
