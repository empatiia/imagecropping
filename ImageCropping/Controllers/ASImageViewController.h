//
//  ASImageViewController.h
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASImageViewController : UIViewController
@property (nonatomic, strong) UIImage *image;

- (id)initWithImage:(UIImage *)image;
@end
