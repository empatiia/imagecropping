//
//  ASViewController.m
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import "ASViewController.h"
#import "ASImageViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ASViewController ()
@property (nonatomic, strong) NSArray *dataSourceArray;
@property (nonatomic, strong) ASImageViewController *imageViewController;
@end

#define kCellReuseIdentifier @"StyleShoots"

@implementation ASViewController

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"TITLE", nil);
    [self.collectionView registerClass:[UICollectionViewCell class]
            forCellWithReuseIdentifier:kCellReuseIdentifier];
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-pattern"]];
}

#pragma mark - Lazy Instantation
- (NSArray *)dataSourceArray
{
    if (!_dataSourceArray)
    {
        _dataSourceArray = [NSArray arrayWithObjects:
                            [UIImage imageNamed:@"scarf"],
                            [UIImage imageNamed:@"scarf"], nil];
    }
    
    return _dataSourceArray;
}

- (ASImageViewController *)imageViewController
{
    if (!_imageViewController)
    {
        _imageViewController = [[ASImageViewController alloc] init];
    }
    
    return _imageViewController;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.imageViewController.image = [self.dataSourceArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:self.imageViewController
                                         animated:YES];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark - UICollectionView DataSource
- (NSInteger)collectionView:(UICollectionView *)view
     numberOfItemsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier
                                                                           forIndexPath:indexPath];

    
    if (!cell)
    {
        cell = [[UICollectionViewCell alloc] init];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[self.dataSourceArray objectAtIndex:indexPath.row]];
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.backgroundView.layer.borderColor = [UIColor blackColor].CGColor;
    cell.layer.borderWidth = 1.0f;
    [cell setBackgroundView:imageView];
    
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100.0f, 100.0f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(50.0f, 20.0f, 50.0f, 20.0f);
}

@end
