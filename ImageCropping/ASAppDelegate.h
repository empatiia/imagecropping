//
//  ASAppDelegate.h
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASViewController;

@interface ASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ASViewController *viewController;

@end
