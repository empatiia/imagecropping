//
//  main.m
//  ImageCropping
//
//  Created by Alex on 14/05/13.
//  Copyright (c) 2013 Alex Salom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASAppDelegate class]));
    }
}
